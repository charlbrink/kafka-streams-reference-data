package com.example.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.schema.avro.AvroSchemaMessageConverter;
import org.springframework.cloud.stream.schema.client.ConfluentSchemaRegistryClient;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.handler.annotation.SendTo;

import java.util.HashMap;
import java.util.Map;

import static com.example.kafka.KStreamProcessorX.DEPARTMENT_FEED;
import static com.example.kafka.KStreamProcessorX.DEPARTMENT_MANAGER_FEED;
import static com.example.kafka.KStreamProcessorX.DEPARTMENT_MANAGER_OUTPUT;
import static com.example.kafka.KStreamProcessorX.MANAGER_FEED;

@Slf4j
@SpringBootApplication
@EnableBinding(KStreamProcessorX.class)
public class KafkaKTableJoinSample {

    public static void main(String[] args) {
        SpringApplication.run(KafkaKTableJoinSample.class, args);
    }

    @StreamListener
    @SendTo(DEPARTMENT_MANAGER_OUTPUT)
    public KStream<String, DepartmentManager> process(@Input(MANAGER_FEED) KTable<String, Manager> managerTable, @Input(DEPARTMENT_FEED) KTable<String, Department> departmentTable) {

        return managerTable.toStream()
                .selectKey((s, manager) -> manager.getDepartment())
                .leftJoin(departmentTable, (manager, department) ->
                        DepartmentManager.newBuilder()
                                .setEmployeeIdentification(manager.getEmployeeIdentification())
                                .setFirstName(manager.getFirstName())
                                .setLastName(manager.getLastName())
                                .setDepartmentCode(department.getIdentification())
                                .setDepartmentName(department.getDescription())
                                .build())
                .selectKey((s, departmentManager) -> departmentManager.getEmployeeIdentification());
    }

    @StreamListener(DEPARTMENT_MANAGER_FEED)
    public void processOutput(DepartmentManager departmentManager) {
        log.info("Sent: [{}] [{}] [{}] [{}]", departmentManager.getEmployeeIdentification(), departmentManager.getFirstName(), departmentManager.getLastName(), departmentManager.getDepartmentName());
    }

    @Configuration
    @EnableKafkaStreams
    static class ApplicationConfiguration {
        @Autowired
        private KafkaProperties kafkaProperties;

        @Value("${spring.cloud.stream.schemaRegistryClient.endpoint}")
        private String schemaRegistryUrl;

        @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
        public StreamsConfig streamsConfigs() {
            Map<String, Object> props = new HashMap<>();
            props.put(StreamsConfig.APPLICATION_ID_CONFIG, kafkaProperties.getProperties().get(StreamsConfig.APPLICATION_ID_CONFIG));
            props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, kafkaProperties.getProperties().get(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG));
            props.put(SslConfigs.SSL_PROTOCOL_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_PROTOCOL_CONFIG));
            props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG));
            props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG));
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG));
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG));
            return new StreamsConfig(props);
        }

        @Bean
        public MessageConverter departmentMessageConverter() {
            AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter();
            converter.setSchema(Department.SCHEMA$);
            return converter;
        }

        @Bean
        public MessageConverter managerMessageConverter() {
            AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter();
            converter.setSchema(Manager.SCHEMA$);
            return converter;
        }

        @Bean
        public MessageConverter departmentManagerMessageConverter() {
            AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter();
            converter.setSchema(DepartmentManager.SCHEMA$);
            return converter;
        }

        @Bean
        public SchemaRegistryClient schemaRegistryClient(@Value("${spring.cloud.stream.schemaRegistryClient.endpoint}") String endpoint){
            ConfluentSchemaRegistryClient client = new ConfluentSchemaRegistryClient();
            client.setEndpoint(endpoint);
            return client;
        }
    }

}

interface KStreamProcessorX {

    String MANAGER_FEED = "managerFeed";
    String DEPARTMENT_FEED = "departmentFeed";
    String DEPARTMENT_MANAGER_OUTPUT = "departmentManagerOutput";
    String DEPARTMENT_MANAGER_FEED = "departmentManagerFeed";

    @Input(MANAGER_FEED)
    KTable<String, Manager> managerFeed();

    @Input(DEPARTMENT_FEED)
    KTable<String, Department> departmentFeed();

    @Output(DEPARTMENT_MANAGER_OUTPUT)
    KStream<String, DepartmentManager> departmentManagerOutput();

    @Input(DEPARTMENT_MANAGER_FEED)
    SubscribableChannel departmentManagerFeed();
}

