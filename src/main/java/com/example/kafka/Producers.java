package com.example.kafka;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class Producers {

    public static final String HUMAN_RESOURCES_MANAGEMENT = "HRM001";
    public static final String FINANCE = "FIN001";
    public static final String MARKETING = "MAR001";
    public static final String PURCHASING = "PUR001";

    public static void main(String... args) {

        final Map<String, String> serdeConfig = Collections.singletonMap(
                AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        final SpecificAvroSerializer<Department> departmentSerializer = new SpecificAvroSerializer<>();
        departmentSerializer.configure(serdeConfig, false);

        Map<String, Object> props = new HashMap<>();
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, departmentSerializer.getClass());

        DefaultKafkaProducerFactory<String, Department> pfDepartment = new DefaultKafkaProducerFactory<>(props);
        KafkaTemplate<String, Department> templateDepartment = new KafkaTemplate<>(pfDepartment, true);
        templateDepartment.setDefaultTopic(KStreamProcessorX.DEPARTMENT_FEED);

        final List<Department> departmentList = Arrays.asList(
                Department.newBuilder().setIdentification(HUMAN_RESOURCES_MANAGEMENT).setDescription("Human Resources Management").build(),
                Department.newBuilder().setIdentification(FINANCE).setDescription("Finance and Accounting").build(),
                Department.newBuilder().setIdentification(MARKETING).setDescription("Marketing").build(),
                Department.newBuilder().setIdentification(PURCHASING).setDescription("Purchasing").build()
        );

        departmentList.forEach(department -> {
            log.info("Writing department for [{}]-[{}]", department.getIdentification(), department.getDescription());
            templateDepartment.sendDefault(department.getIdentification(), department);
        });

        final List<Manager> managers = Arrays.asList(
                Manager.newBuilder().setEmployeeIdentification("E0001").setFirstName("FirstName1").setLastName("LastName1").setDepartment(HUMAN_RESOURCES_MANAGEMENT).build(),
                Manager.newBuilder().setEmployeeIdentification("E0002").setFirstName("FirstName2").setLastName("LastName2").setDepartment(FINANCE).build(),
                Manager.newBuilder().setEmployeeIdentification("E0003").setFirstName("FirstName3").setLastName("LastName3").setDepartment(MARKETING).build(),
                Manager.newBuilder().setEmployeeIdentification("E0004").setFirstName("FirstName4").setLastName("LastName4").setDepartment(PURCHASING).build(),
                Manager.newBuilder().setEmployeeIdentification("E0005").setFirstName("FirstName5").setLastName("LastName5").setDepartment(PURCHASING).build(),
                Manager.newBuilder().setEmployeeIdentification("E0006").setFirstName("FirstName6").setLastName("LastName6").setDepartment(MARKETING).build()
        );

        final SpecificAvroSerializer<Manager> managerSerializer = new SpecificAvroSerializer<>();
        managerSerializer.configure(serdeConfig, false);

        Map<String, Object> props1 = new HashMap<>(props);
        props1.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props1.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, managerSerializer.getClass());

        DefaultKafkaProducerFactory<String, Manager> pfManager = new DefaultKafkaProducerFactory<>(props1);
        KafkaTemplate<String, Manager> templateManager = new KafkaTemplate<>(pfManager, true);
        templateManager.setDefaultTopic(KStreamProcessorX.MANAGER_FEED);

        managers.forEach(manager -> {
            log.info("Writing manager for [{}]-[{}]", manager.getDepartment(), manager.getFirstName(), manager.getLastName());
            templateManager.sendDefault(manager.getEmployeeIdentification(), manager);
        });

    }

}