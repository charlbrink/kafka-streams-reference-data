# Spring CloudKafka Streams Sample

## Prerequisites

Kafka, Zookeeper and Confluent Schema Registry

Installation using docker compose:
```
$ cd src/main/docker
$ docker-compose up -d
```   

## Overview

Example project to left join reference data KTables ("managerFeed", "departmentFeed").
The feeds are joined and output to a new topic "departmentManagerFeed".

All data is stored in Kafka in Avro format, see schemas in src/main/resources/avro.

